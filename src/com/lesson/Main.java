package com.lesson;

public class Main {

    public static void main(String[] args) {

        //Задание 1
        int n1;
        n1 = 5;
        System.out.println("Базовый синтаксис. Задание 1. Число :" + n1);
        //Задание 2
        int n2;
        n2 = 10;
        n2 += 2;
        System.out.println("Базовый синтаксис. Задание 2. Число :" + n2);
        //Задание 3
        int n3;
        n3 = 15;
        n3 -= 4;
        System.out.println("Базовый синтаксис. Задание 3. Число :" + n3);
        //Задание 4
        int n4;
        n4 = 20;
        n4 *= 6;
        System.out.println("Базовый синтаксис. Задание 4. Число :" + n4);
        //Задание 5
        int n5;
        n5 = 20;
        n5 /= 6;
        System.out.println("Базовый синтаксис. Задание 5. Число :" + n5);
        //Задание 6
        long lg1;
        lg1 = 20L;
        lg1 /= 2;
        System.out.println("Базовый синтаксис. Задание 6. Число :" + lg1);
        //Задание 7
        long lg2;
        lg2 = 20L;
        lg2 %= 5;
        System.out.println("Базовый синтаксис. Задание 7. Число :" + lg2);
        //Задание 8
        float ft1;
        ft1 = 19F;
        ft1 = ft1 / 3 + 6;
        System.out.println("Базовый синтаксис. Задание 8. Число :" + ft1);
        //Задание 9
        double db1;
        db1 = 10_000D;
        db1 = (db1 - 5) * 6;
        System.out.println("Базовый синтаксис. Задание 9. Число :" + db1);
        //Задание 10
        int n10;
        n10 = 105;
        n10 += 1;
        n10++;
        n10 = n10 + 1;
        System.out.println("Базовый синтаксис. Задание 10. Число :" + n10);

        //Условные операции
        //Задание 1
        int nn1;
        nn1 = 15;
        if (nn1 > 0) {
            System.out.println("Условные операции. Задание 1. Число " + nn1 + " больше нуля.");
        }
        //Задание 2
        float ff1;
        ff1 = -150F;
        if (ff1 < 0) {
            System.out.println("Условные операции. Задание 2. Number " + ff1 + " is negative.");
        }
        //Задание 3
        int nn3;
        nn3 = 5;
        if (nn3 == 5) {
            System.out.println("Условные операции. Задание 3. It's true.");
        }
        //Задание 4
        int nn4;
        int nn5;
        nn4 = 15;
        nn5 = 5;

        if (nn4 > nn5) {
            System.out.println("Условные операции. Задание 4. Наибольшее число: " + nn4);
        } else if (nn5 > nn4) {
            System.out.println("Условные операции. Задание 4. Наибольшее число: " + nn5);
        }
        //Задание 5
        int nn44;
        int nn55;
        nn44 = 15;
        nn55 = 5;

        if (nn44 < nn55) {
            System.out.println("Условные операции. Задание 5. Наименьшее число: " + nn44);
        } else if (nn55 < nn44) {
            System.out.println("Условные операции. Задание 5. Наименьшее число: " + nn55);
        }
        //Задание 6
        int n66;
        int n77;
        n66 = 1105;
        n77 = 215;

        if (n66 < n77) {
            System.out.println("Условные операции. Задание 6. Min=" + n66 + ", max=" + n77);
        } else {
            System.out.println("Условные операции. Задание 6. Min=" + n77 + ", max=" + n66);
        }
        //Задание 7
        int nn66;
        int nn77;
        nn66 = 2045;
        nn77 = 245;

        if (nn66 < nn77) {
            System.out.println("Условные операции. Задание 7. Min=" + nn66 + ", max=" + nn77);
        } else if (nn77 < nn66) {
            System.out.println("Условные операции. Задание 7. Min=" + nn77 + ", max=" + nn66);
        } else {
            System.out.println("Условные операции. Задание 7. Numbers " + nn66 + " and " + nn77 + " is equal.");
        }
        //Задание 8
        float f27;
        float f28;

        f27 = 200.5f;
        f28 = -20.5f;

        if (f27 > 0) {
            System.out.println("Условные операции. Задание 8. Положительное число:" + f27);
        } else if (f28 > 0) {
            System.out.println("Условные операции. Задание 8. Положительное число:" + f28);
        }
        //Задание 9
        float ff27;
        float ff28;

        ff27 = 2430.5f;
        ff28 = -2440.3f;

        if (ff27 < 0) {
            System.out.println("Условные операции. Задание 9. Отрицательное число:" + ff27);
        } else if (ff28 < 0) {
            System.out.println("Условные операции. Задание 9. Отрицательное число:" + ff28);
        }
        //Задание 10
        float f10;
        float f11;

        f10 = 43230.5f;
        f11 = -332440.3f;

        if (f10 < 0) {
            System.out.println("Условные операции. Задание 10. Отрицательное число:" + f10);
        } else {
            System.out.println("Условные операции. Задание 10. Положительное число:" + f10);
        }
        if (f11 < 0) {
            System.out.println("Условные операции. Задание 10. Отрицательное число:" + f11);
        } else {
            System.out.println("Условные операции. Задание 10. Положительное число:" + f11);
        }

        //Задание 11
        double ff10;
        double ff11;

        ff10 = 0.0d;
        ff11 = -540.3d;

        if (ff10 != 0) {
            System.out.println("Условные операции. Задание 11. Число:" + ff10 + " не равно нулю.");
        }
        if (ff11 != 0) {
            System.out.println("Условные операции. Задание 11. Число:" + ff11 + " не равно нулю.");
        }


        //TASK1
        int Germany = 82_800_000;
        int UKingdom = 65_808_573;
        int France = 67_024_459;
        int Italy = 60_589_445;

        System.out.println("Task 1. Германия:" + Germany + " человек.");
        System.out.println("Task 1. Великобритания:" + UKingdom + " человек.");
        System.out.println("Task 1. Франция:" + France + " человек.");
        System.out.println("Task 1. Италия:" + Italy + " человек.");

        //TASK2
        long Germany1 = 357_021L;//sq km
        long UKingdom1 = 244_820;
        long France1 = 547_030;
        long Italy1 = 301_340;
        //const
        int sqm = 1_000_000;
        float sqmiles = 0.38610215854245F;

        System.out.println("Task 2.Площадь Германии:" + Germany1 * sqm + "кв.м.");
        System.out.println("Task 2.Площадь Германии:" + Germany1 * sqmiles + "кв.миль.");

        System.out.println("Task 2.Площадь Великобритании:" + UKingdom1 * sqm + "кв.м.");
        System.out.println("Task 2.Площадь Великобритании:" + UKingdom1 * sqmiles + "кв.миль.");

        System.out.println("Task 2.Площадь Франции:" + France1 * sqm + "кв.м.");
        System.out.println("Task 2.Площадь Франции:" + France1 * sqmiles + "кв.миль.");

        System.out.println("Task 2.Площадь Италии:" + Italy1 * sqm + "кв.м.");
        System.out.println("Task 2.Площадь Италии:" + Italy1 * sqmiles + "кв.миль.");

        //TASK3
        //EUR, USD, RUB sales
        float[] currency1 = {31.7F, 26.8F, 0.46F};
        float sum = 5000F;

        System.out.println("Task 3.Конвертер: " + sum + " грн, это - " + sum / currency1[0] + " EUR.");
        System.out.println("Task 3.Конвертер: " + sum + " грн, это - " + sum / currency1[1] + " USD.");
        System.out.println("Task 3.Конвертер: " + sum + " грн, это - " + sum / currency1[2] + " RUB.");


    }
}


